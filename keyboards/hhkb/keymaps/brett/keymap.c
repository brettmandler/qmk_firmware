/*  -*-  eval: (turn-on-orgtbl); -*-
 * Brettm12345 HHKB Layout
 */
#include QMK_KEYBOARD_H

#define BASE 0
#define HHKB 1
#define PROG 2
#define MOUSE 3

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

    /* BASE Level: Default Layer
     |----------+---------+---+---+---+---+---+---+---+---+--------+---------+----------+------------+---|
     | Esc/~    | 1       | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 0      | -       | =        | \          | ` |
     |----------+---------+---+---+---+---+---+---+---+---+--------+---------+----------+------------+---|
     | Tab/Prog | Q       | W | E | R | T | Y | U | I | O | P      | [       | ]        | Backs/Prog |   |
     |----------+---------+---+---+---+---+---+---+---+---+--------+---------+----------+------------+---|
     | Esc/Cont | A/Mouse | S | D | F | G | H | J | K | L | ;/Prog | '       | Ent/Cont |            |   |
     |----------+---------+---+---+---+---+---+---+---+---+--------+---------+----------+------------+---|
     | (/Shift  | Z       | X | C | V | B | N | M | , | . | /      | )/Shift | HHKB     |            |   |
     |----------+---------+---+---+---+---+---+---+---+---+--------+---------+----------+------------+---|

            |--------+--------+-----------------------+--------+--------|
            | [/LAlt | {/LGUI | ******* Space ******* | ]/RGUI | }/RAlt |
            |--------+--------+-----------------------+--------+--------|
    */

	[BASE] = LAYOUT(
    KC_GESC, KC_1, KC_2, KC_3, KC_4, KC_5, KC_6, KC_7, KC_8, KC_9, KC_0, KC_MINS, KC_EQL, KC_BSLS, KC_GRV,
    LT(PROG,KC_TAB), KC_Q, KC_W, KC_E, KC_R, KC_T, KC_Y, KC_U, KC_I, KC_O, KC_P, KC_LBRC, KC_RBRC, LT(PROG,KC_BSPC),
    LCTL(KC_ESC), LT(MOUSE,KC_A), KC_S, KC_D, KC_F, KC_G, KC_H, KC_J, KC_K, KC_L, LT(PROG,KC_SCLN), KC_QUOT, RCTL(KC_ENT),
    KC_LSPO, KC_Z, KC_X, KC_C, KC_V, KC_B, KC_N, KC_M, KC_COMM, KC_DOT, KC_SLSH, KC_RSPC, MO(HHKB), LALT(KC_LPRN),
    LGUI(KC_LBRC), /*     */ KC_SPC, RGUI(KC_RBRC), RALT(KC_RPRN)),

	[HHKB] = LAYOUT(
    KC_PWR, KC_F1, KC_F2, KC_F3, KC_F4, KC_F5, KC_F6, KC_F7, KC_F8, KC_F9, KC_F10, KC_F11, KC_F12, KC_INS, KC_DEL,
    KC_CAPS, KC_CALC, KC_MAIL, KC_MSEL, KC_WREF, KC_MYCM, KC_WFAV, KC_TRNS, KC_TRNS, KC_TRNS, KC_PSCR, KC_TRNS, KC_TRNS, KC_BSPC, KC_TRNS, KC_MPRV, KC_MNXT, KC_FIND, KC_TRNS, KC_TRNS, KC_WBAK, KC_WHOM, KC_WSCH, KC_WFWD, KC_TRNS, KC_TRNS, KC_EXEC,
    KC_TRNS, KC__VOLUP, KC_VOLD, KC_MUTE, KC_SLCT, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_AGIN, KC_WSCH, KC_TRNS, KC_TRNS,
    KC_MENU, KC_TRNS, KC_MPLY, KC_TRNS, KC_MENU),

	[PROG] = LAYOUT(
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_EXLM, KC_AT, KC_LCBR, KC_RCBR, KC_PAST, KC_COPY, KC_UNDO, KC_TRNS, KC_PEQL, KC_PSTE, KC_PLUS, KC_PMNS, KC_TRNS, KC_TRNS, KC_HASH, KC_DLR, KC_LPRN, KC_RPRN, KC_PSLS, KC_LEFT, KC_DOWN, KC_UP, KC_RGHT, KC_BSLS, KC_TILD, KC_TRNS,
    KC_TRNS, KC_PERC, KC_CIRC, KC_LBRC, KC_RBRC, KC_PIPE, KC_TRNS, KC_TRNS, KC_TRNS, KC_SCLN, KC_COLN, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS),

	[MOUSE] = LAYOUT(
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_MS_L, KC_MS_D, KC_MS_U, KC_MS_R, KC_BTN1, KC_BTN2, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_BTN5, KC_BTN4, KC_BTN3, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS)};
